//
//  LoginViewController.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-02-14.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *loginWithLinkedInButton;

@end
