//
//  Person.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-06.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "Person.h"

@implementation Person

+ (Person *)initWithDictionary:(NSMutableDictionary *)array
{
    Person *person = [[Person alloc] init];
    person.currentEventName = [array objectForKey:@"currentEventName"];
    person.fullName = [array objectForKey:@"full_name"];
    person.linkedinURL = [array objectForKey:@"linkedinURL"];
    person.occupation = [array objectForKey:@"occupation"];
    person.profilePictureURL = [array objectForKey:@"profilePic"];
    
    
    person.skills = [[NSMutableArray alloc] init];
    for(NSDictionary *skillDict in [array valueForKey:@"skills"]) {
        NSString *skill = [[skillDict valueForKey:@"skill"] valueForKey:@"name"];
        [person.skills addObject:skill];
    }
    
    person.connections = [[NSMutableArray alloc] init];
    for(NSDictionary *connectionDict in [array valueForKey:@"connections"]) {
        NSString *connection = [NSString stringWithFormat:@"%@ %@",[connectionDict valueForKey:@"firstName"],[connectionDict valueForKey:@"lastName"]];
        [person.connections addObject:connection];
    }
    
    return person;
}


@end
