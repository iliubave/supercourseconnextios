//
//  PersonDiscoveryTableViewCell.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-06.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface PersonDiscoveryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *connectButtonPressed;
@property (strong, nonatomic) IBOutlet UILabel *fullname;
@property (strong, nonatomic) IBOutlet UILabel *occupation;
@property (strong, nonatomic) IBOutlet AsyncImageView *profilePhoto;

@end
