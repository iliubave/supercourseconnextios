//
//  HashtagPaidAPI.h
//  launchboxTO
//
//  Created by Igar Liubavetskiy on 2014-12-10.
//  Copyright (c) 2014 Igar Liubavetskiy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnextAPIDelegate.h"

@protocol ConnextApiDelegate;

@interface ConnextAPI : NSObject<NSURLConnectionDelegate>
@property (weak, nonatomic) id<ConnextApiDelegate> delegate;

- (void)submitGeoLocation;
- (void)registerUser:(NSString *)user forEventNamed:(NSString *)event;
- (void)usersNearGeoLocation:(NSString *)location;
- (void)usersRegisteredForEvent:(NSString *)event;
- (void)registerUserWithEmail;


- (void)listOfAttendeesForEvent:(NSString *)eventName;
- (void)registerUserWithID:(NSString *)userID forEventNamed:(NSString *)eventName;
- (void)personalInformationForUserWithID:(NSString *)userID;
- (void)registerUserWithID:(NSString *)userID fullName:(NSString *)fullName profilePictureURL:(NSString *)url occupation:(NSString *)occupation linkedinURL:(NSString *)linkedinURL skills:(NSDictionary *)skills connections:(NSDictionary *)connections;

@end
    
