//
//  ConnectionsViewController.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-28.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnextAPIDelegate.h"

@interface ConnectionsViewController : UIViewController <ConnextApiDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *peopleCollectionView;
@end
