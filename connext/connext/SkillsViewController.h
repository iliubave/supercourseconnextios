//
//  SkillsViewController.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-22.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkillsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *skillsTextView;
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;

@end
