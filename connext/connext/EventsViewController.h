//
//  EventsViewController.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-29.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *mapImageView;
@property (strong, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *eventDateLabel;

@end
