//
//  HashtagPaidAPIDelegate.h
//  launchboxTO
//
//  Created by Igar Liubavetskiy on 2014-12-10.
//  Copyright (c) 2014 Igar Liubavetskiy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ConnextApiDelegate <NSObject>

- (void)receivedPersons:(NSData *)objectNotation;
- (void)received:(NSData *)objectNotation;
- (void)fetchingFailed:(NSError *)error;

@end
