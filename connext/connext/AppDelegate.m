//
//  AppDelegate.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-02-10.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "AppDelegate.h"
#import "FCCurrentLocationGeocoder.h"

@interface AppDelegate ()

@end

@implementation AppDelegate {
    FCCurrentLocationGeocoder *geocoder;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    you can use the shared instance
//    [FCIPAddressGeocoder setDefaultService:FCIPAddressGeocoderServiceTelize];
//    [FCCurrentLocationGeocoder sharedGeocoder];
//    
//    //you can also use as many shared instances as you need
//    [FCCurrentLocationGeocoder sharedGeocoderForKey:@"yourKey"];
//    
//    //or create a new geocoder and set options
//    geocoder = [FCCurrentLocationGeocoder new];
//    geocoder.canPromptForAuthorization = NO; //(optional, default value is YES)
//    geocoder.canUseIPAddressAsFallback = YES; //(optional, default value is NO. very useful if you need just the approximate user location, such as current country, without asking for permission)
//    geocoder.timeFilter = 30; //(cache duration, optional, default value is 5 seconds)
//    geocoder.timeoutErrorDelay = 10; //(optional, default value is 15 seconds)
//    
//    [self startTimedTask];
    
    return YES;
}

- (void)startTimedTask
{
    NSTimer *fiveSecondTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateCurrentGeoLocation) userInfo:nil repeats:YES];
}

- (void)updateCurrentGeoLocation {
    //current-location forward-geocoding
    [geocoder geocode:^(BOOL success) {
        
        if(success)
        {
            //you can access the current location using 'geocoder.location'
            NSLog(@"%@",geocoder.location);
        }
        else {
            //you can debug what's going wrong using: 'geocoder.error'
            NSLog(@"%@",geocoder.error);
        }
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
