//
//  ProfileViewController.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-28.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "ProfileViewController.h"
#import "AsyncImageView.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [_profileImageView setImageURL:[NSURL URLWithString:[prefs valueForKey:@"pic"]]];
    [_userFullname setText:[NSString stringWithFormat:@"%@ %@", [prefs valueForKey:@"firstName"], [prefs valueForKey:@"lastName"]]];
    [_userOccupation setText:[prefs valueForKey:@"currentOccupation"]];
    [_currentEvent setText:@"Current event: Sample Event"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
