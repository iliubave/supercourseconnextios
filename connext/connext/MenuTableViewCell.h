//
//  MenuTableViewCell.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-26.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *optionLabel;
@property (strong, nonatomic) IBOutlet UILabel *optionDescription;

@end
