//
//  DiscoveryViewController.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-02-23.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "DiscoveryViewController.h"
#import "FCCurrentLocationGeocoder.h"
#import "ConnextAPI.h"
#import "ConnextAPI.h"
#import "Person.h"
#import "PersonDiscoveryTableViewCell.h"
#import "AsyncImageView.h"
#import "RGCollectionViewCell.h"

@interface DiscoveryViewController ()

@end

@implementation DiscoveryViewController {
    FCCurrentLocationGeocoder *geocoder;
    NSMutableArray *peopleIDs;
    NSMutableArray *persons;
    ConnextAPI *api_manager;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    peopleIDs = [[NSMutableArray alloc] init];
    persons = [[NSMutableArray alloc] init];
    
    api_manager = [[ConnextAPI alloc] init];
    api_manager.delegate = self;
    
//    _peopleTableView.estimatedRowHeight = 44.0;
//    _peopleTableView.rowHeight = UITableViewAutomaticDimension;
//    _peopleTableView.delegate = self;
//    _peopleTableView.dataSource = self;
    
//    [_peopleCollectionView registerClass:[RGCollectionViewCell class]
//             forCellWithReuseIdentifier:@"reuse"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [api_manager listOfAttendeesForEvent:@"Sample event"];
}

- (void)received:(NSData *)objectNotation {
    if(objectNotation != (id)[NSNull null]) {
        [peopleIDs removeAllObjects];
        [persons removeAllObjects];
        for(NSString *message in objectNotation) {
            [peopleIDs addObject:message];
        }
    }
    
    for(NSString *userID in peopleIDs) {
        [api_manager personalInformationForUserWithID:userID];
    }
}


- (void)receivedPersons:(NSData *)objectNotation {
    if(objectNotation != (id)[NSNull null]) {
        [persons addObject:[Person initWithDictionary:objectNotation]];
    }
    [_peopleCollectionView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  [persons count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RGCollectionViewCell *cell = (RGCollectionViewCell  *)[collectionView dequeueReusableCellWithReuseIdentifier:@"reuse" forIndexPath:indexPath];
    [self configureCell:cell withIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(RGCollectionViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.linkedInURL = [(Person *)[persons objectAtIndex:indexPath.section] linkedinURL];
    cell.profileFullname.text = [(Person *)[persons objectAtIndex:indexPath.section] fullName];
    cell.profileOccupation.text = [(Person *)[persons objectAtIndex:indexPath.section] occupation];
    
    cell.profileImageView.image = nil;
    cell.profileImageView.imageURL = [NSURL URLWithString:[(Person *)[persons objectAtIndex:indexPath.section] profilePictureURL]];
    
    // case for the very first cell
    if(indexPath.section == 0) {
        [cell.progressView setProgress:[(Person *)[persons objectAtIndex:indexPath.section] percentage]/100.0 animated:YES];
        cell.shouldAnimate = false;
    }
    
    [cell.progressView setProgress:0 animated:NO];
    cell.shouldAnimate = false;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    for (UICollectionViewCell *cell in [self.peopleCollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.peopleCollectionView indexPathForCell:cell];
        
        RGCollectionViewCell *cell = (RGCollectionViewCell *)[_peopleCollectionView cellForItemAtIndexPath:indexPath];
        
        [cell.progressView setProgress:[(Person *)[persons objectAtIndex:indexPath.section] percentage]/100.0 animated:YES];
        
        cell.shouldAnimate = false;
    }
}

@end
