//
//  DiscoveryViewController.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-02-23.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnextAPIDelegate.h"

@interface DiscoveryViewController : UIViewController <ConnextApiDelegate, UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *peopleCollectionView;

@end
