//
//  SkilledDiscoveryViewController.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-29.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "SkilledDiscoveryViewController.h"
#import "ConnextAPI.h"
#import "Person.h"
#import "RGCollectionViewCell.h"
#import "AsyncImageView.h"

@interface SkilledDiscoveryViewController ()

@end

@implementation SkilledDiscoveryViewController {
    NSMutableArray *peopleIDs;
    NSMutableArray *persons;
    ConnextAPI *api_manager;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    peopleIDs = [[NSMutableArray alloc] init];
    persons = [[NSMutableArray alloc] init];
    
    api_manager = [[ConnextAPI alloc] init];
    api_manager.delegate = self;
    
    //    _peopleTableView.estimatedRowHeight = 44.0;
    //    _peopleTableView.rowHeight = UITableViewAutomaticDimension;
    //    _peopleTableView.delegate = self;
    //    _peopleTableView.dataSource = self;
    
    //    [_peopleCollectionView registerClass:[RGCollectionViewCell class]
    //             forCellWithReuseIdentifier:@"reuse"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [api_manager listOfAttendeesForEvent:@"Sample event"];
}

- (void)received:(NSData *)objectNotation {
    if(objectNotation != (id)[NSNull null]) {
        [peopleIDs removeAllObjects];
        [persons removeAllObjects];
        for(NSString *message in objectNotation) {
            [peopleIDs addObject:message];
        }
    }
    
    for(NSString *userID in peopleIDs) {
        [api_manager personalInformationForUserWithID:userID];
    }
}


- (void)receivedPersons:(NSData *)objectNotation {
    if(objectNotation != (id)[NSNull null]) {
        [persons addObject:[Person initWithDictionary:objectNotation]];
    }
    
    [self sortPeopleAccordingToDesiredSkills];
    [_peopleCollectionView reloadData];
}

- (void)sortPeopleAccordingToDesiredSkills {
    for(Person *person in persons) {
        int match = 0;
        
        for(NSString *skill in person.skills) {
            if([_desiredSkills containsObject:[skill lowercaseString]])
                match++;
        }
        
        person.percentage = 1.0 * match/[_desiredSkills count]*100;
    }
    
    // Sort the attendees by the percentage values
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"percentage" ascending:NO];
    [persons sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  [persons count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RGCollectionViewCell *cell = (RGCollectionViewCell  *)[collectionView dequeueReusableCellWithReuseIdentifier:@"reuse" forIndexPath:indexPath];
    [self configureCell:cell withIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(RGCollectionViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.profileFullname.text = [(Person *)[persons objectAtIndex:indexPath.section] fullName];
    cell.profileOccupation.text = [(Person *)[persons objectAtIndex:indexPath.section] occupation];
    
    cell.profileImageView.image = nil;
    cell.profileImageView.imageURL = [NSURL URLWithString:[(Person *)[persons objectAtIndex:indexPath.section] profilePictureURL]];
    
    // case for the very first cell
    if(indexPath.section == 0) {
        [cell.progressView setProgress:[(Person *)[persons objectAtIndex:indexPath.section] percentage]/100.0 animated:YES];
        cell.shouldAnimate = false;
    }
    
    [cell.progressView setProgress:0 animated:NO];
    cell.shouldAnimate = false;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    for (UICollectionViewCell *cell in [self.peopleCollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.peopleCollectionView indexPathForCell:cell];
        
        RGCollectionViewCell *cell = (RGCollectionViewCell *)[_peopleCollectionView cellForItemAtIndexPath:indexPath];
        
        [cell.progressView setProgress:[(Person *)[persons objectAtIndex:indexPath.section] percentage]/100.0 animated:YES];
        
        cell.shouldAnimate = false;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
