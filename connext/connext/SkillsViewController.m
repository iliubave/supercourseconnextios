//
//  SkillsViewController.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-22.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "SkillsViewController.h"
#import "SkilledDiscoveryViewController.h"

@interface SkillsViewController ()

@end

@implementation SkillsViewController {
    NSMutableArray *desiredSkills;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    desiredSkills = [[NSMutableArray alloc] init];
    
    // Set the background and shadow image to get rid of the line.
    [_navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    _navigationBar.shadowImage = [[UIImage alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)searchWithSkills:(id)sender {
    NSArray *rawSkills = [_skillsTextView.text componentsSeparatedByString:@","];
    NSMutableArray *skills = [[NSMutableArray alloc] init];
    
    for(NSString *skill in rawSkills) {
        if([skill characterAtIndex:0] == ' ') {
            [skills addObject:[[skill stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString]];
        } else {
            [skills addObject:[skill lowercaseString]];
        }
    }
    
    desiredSkills = skills;
    
}
- (IBAction)nextButtonTapped:(id)sender {
    NSArray *skillInputs = [[_skillsTextView text] componentsSeparatedByString:@", "];
    desiredSkills = [NSMutableArray arrayWithArray:skillInputs];
    
    [self performSegueWithIdentifier:@"toSkilledDiscovery" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [(SkilledDiscoveryViewController *)segue.destinationViewController setDesiredSkills:desiredSkills];
}

@end
