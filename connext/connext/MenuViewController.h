//
//  MenuViewController.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-26.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;

@end
