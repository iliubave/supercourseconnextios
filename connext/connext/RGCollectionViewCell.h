//
//  RGCollectionViewCell.h
//  RGCardViewLayout
//
//  Created by ROBERA GELETA on 1/23/15.
//  Copyright (c) 2015 ROBERA GELETA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "M13ProgressViewRing.h"

@interface RGCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *profileFullname;
@property (weak, nonatomic) IBOutlet UILabel *profileOccupation;
@property (weak, nonatomic) IBOutlet M13ProgressViewRing *progressView;
@property (weak, nonatomic) IBOutlet NSString *linkedInURL;
@property BOOL shouldAnimate;
- (IBAction)openInBrowser:(id)sender;


@end
