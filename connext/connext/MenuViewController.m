//
//  MenuViewController.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-26.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuTableViewCell.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _menuTableView.estimatedRowHeight = 44.0;
    _menuTableView.rowHeight = UITableViewAutomaticDimension;
    _menuTableView.delegate = self;
    _menuTableView.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuTableViewCell *cell = (MenuTableViewCell  *)[_menuTableView dequeueReusableCellWithIdentifier:@"MenuCell" forIndexPath:indexPath];
    [self configureCell:cell withIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(MenuTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0) {
        cell.optionLabel.text = @"Discover";
        cell.optionDescription.text = @"See all the people attending this event";
    } else if(indexPath.row == 1) {
        cell.optionLabel.text = @"Connections nearby";
        cell.optionDescription.text = @"See all your connections attending this event";
    } else if(indexPath.row == 2) {
        cell.optionLabel.text = @"Skilled discovery";
        cell.optionDescription.text = @"See all the people attending this event with skills that you can personally predefine";
    } else if(indexPath.row == 3) {
        cell.optionLabel.text = @"Profile";
        cell.optionDescription.text = @"Edit/Show your profile";
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0) {
        [self performSegueWithIdentifier:@"toDiscovery" sender:self];
    } else if(indexPath.row == 1) {
        [self performSegueWithIdentifier:@"toChooseSkills" sender:self];
    } else if(indexPath.row == 2) {
        [self performSegueWithIdentifier:@"toChooseSkills" sender:self];
    } else if(indexPath.row == 3) {
        [self performSegueWithIdentifier:@"toChooseSkills" sender:self];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
