//
//  LoginViewController.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-02-14.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "LoginViewController.h"
#import "LIALinkedInApplication.h"
#import "LIALinkedInHttpClient.h"
#import "ConnextAPI.h"

#define LINKEDIN_CLIENT_ID @"786otnoafgyvaa"
#define LINKEDIN_CLIENT_SECRET @"nwxWvH2QrQpaMQbh"

@implementation LoginViewController {
    LIALinkedInHttpClient *_client;
    ConnextAPI *api_manager;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _client = [self client];
    api_manager = [[ConnextAPI alloc] init];
}

- (IBAction)requestAccessToken:(id)sender {
    
    if ([_client validToken]) {
        [self requestMeWithToken:[_client accessToken]];
    } else {
        [_client getAuthorizationCode:^(NSString *code) {
            [self.client getAccessToken:code success:^(NSDictionary *accessTokenData) {
                NSString *accessToken = [accessTokenData objectForKey:@"access_token"];
                [self requestMeWithToken:accessToken];
            }                   failure:^(NSError *error) {
                NSLog(@"Quering accessToken failed %@", error);
            }];
        }                      cancel:^{
            NSLog(@"Authorization was cancelled by user");
        }                     failure:^(NSError *error) {
            NSLog(@"Authorization failed %@", error);
        }];
    }
}

- (void)requestMeWithToken:(NSString *)accessToken {
    [self.client GET:[NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,industry,headline,skills,connections,picture-url::(original),siteStandardProfileRequest,location:(name),positions:(company:(name),title))?oauth2_access_token=%@&format=json", accessToken] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *result) {
        
        // Store the data
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

        NSString *userID = [result valueForKey:@"id"];
        NSString *fullname = [NSString stringWithFormat:@"%@ %@", [result valueForKey:@"firstName"],[result valueForKey:@"lastName"]];
        NSString *pictureURL = [result valueForKey:@"pictureUrl"];
        NSString *occupation = [result valueForKey:@"headline"];
        NSString *linkedinURL = [[result valueForKey:@"siteStandardProfileRequest"] valueForKey:@"url"];
        NSDictionary *skills = [[result valueForKey:@"skills"] valueForKey:@"values"];
        NSDictionary *connections = [[result valueForKey:@"connections"] valueForKey:@"values"];
        
        
        [defaults setObject:[result valueForKey:@"id"] forKey:@"userLinkedInID"];
        [defaults setObject:[result valueForKey:@"firstName"] forKey:@"firstName"];
        [defaults setObject:[result valueForKey:@"lastName"] forKey:@"lastName"];
        [defaults setObject:[result valueForKey:@"pictureUrl"] forKey:@"pic"];
        [defaults setObject:[result valueForKey:@"headline"] forKey:@"currentOccupation"];
        [defaults setObject:[[result valueForKey:@"siteStandardProfileRequest"] valueForKey:@"url"] forKey:@"linkedInProfileLink"];
        [defaults setObject:[[result valueForKey:@"skills"] valueForKey:@"values"] forKey:@"skills"];
        [defaults setObject:[[result valueForKey:@"connections"] valueForKey:@"values"] forKey:@"connections"];
        
        [defaults synchronize];
        
        [api_manager registerUserWithID:userID fullName:fullname profilePictureURL:pictureURL occupation:occupation linkedinURL:linkedinURL skills:skills connections:connections];
        
        [self performSegueWithIdentifier:@"successfullyAuthenticated" sender:self];
    
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"failed to fetch current user %@", error);
    }];
}

- (LIALinkedInHttpClient *)client {
    LIALinkedInApplication *application = [LIALinkedInApplication applicationWithRedirectURL:@"http://www.ryerson.ca"
                                                                                    clientId:LINKEDIN_CLIENT_ID
                                                                                clientSecret:LINKEDIN_CLIENT_SECRET
                                                                                       state:@"DCEEFWF45453sdffef424"
                                                                               grantedAccess:@[@"r_fullprofile", @"r_network"]];
    return [LIALinkedInHttpClient clientForApplication:application presentingViewController:nil];
}

@end
