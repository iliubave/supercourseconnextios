//
//  SkilledDiscoveryViewController.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-29.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnextAPIDelegate.h"

@interface SkilledDiscoveryViewController : UIViewController <ConnextApiDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *peopleCollectionView;
@property (strong, nonatomic) IBOutlet NSArray *desiredSkills;

@end
