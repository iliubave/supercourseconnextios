//
//  Person.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-06.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic,strong) NSString *fullName;
@property (nonatomic,strong) NSString *location;
@property (nonatomic, strong) NSString *occupation;
@property (nonatomic, strong) NSString *profilePictureURL;
@property (nonatomic, strong) NSString *linkedinURL;
@property (nonatomic, strong) NSString *currentEventName;
@property (nonatomic, strong) NSMutableArray *skills;
@property (nonatomic, strong) NSMutableArray *connections;
@property float percentage;

+ (Person *)initWithDictionary:(NSMutableDictionary *)array;

@end
