//
//  RGCollectionViewCell.m
//  RGCardViewLayout
//
//  Created by ROBERA GELETA on 1/23/15.
//  Copyright (c) 2015 ROBERA GELETA. All rights reserved.
//

#import "RGCollectionViewCell.h"

@implementation RGCollectionViewCell

@synthesize profileImageView;
@synthesize profileFullname;
@synthesize profileOccupation;
@synthesize progressView;
@synthesize shouldAnimate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        shouldAnimate = true;
    }
    return self;
}

- (IBAction)openInBrowser:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_linkedInURL]];
}
@end
