//
//  MyViewController.m
//  DropdownMenu-Example
//
//  Created by Nils Mattisson on 1/13/14.
//  Copyright (c) 2014 Nils Mattisson. All rights reserved.
//

#import "MyViewController.h"
#import "ConnextAPI.h"

@interface MyViewController ()

@end

@implementation MyViewController {
    ConnextAPI *api_manager;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    api_manager = [[ConnextAPI alloc] init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *picURL = [prefs stringForKey:@"pic"];
    NSString *first = [prefs stringForKey:@"firstName"];
    NSString *last = [prefs stringForKey:@"lastName"];
    NSString *occupation = [prefs stringForKey:@"currentOccupation"];
    
    //Define the labels with the user's first name, last name and occupation
    self.firstName.text = first;
    self.lastName.text = last;
    self.job.text = occupation;
    
    //Get the user's linkedIn profile picture from the URL
    NSData *profilePicture = [NSData dataWithContentsOfURL:[NSURL URLWithString:picURL]];
    
    
    //Set the image view to display the image as a blurred image
    self.profilePic.image = [UIImage imageWithData:profilePicture];
    self.profilePic.tintColor =[UIColor blueColor];
    
    //Display the actual profile picture image
    self.profileCircle.image = [UIImage imageWithData:profilePicture];
    
    //Display the profile picture image as a circle with a border
    self.profileCircle.layer.cornerRadius = self.profileCircle.frame.size.width / 2;
    self.profileCircle.clipsToBounds = YES;
    self.profileCircle.layer.borderWidth = 3.0f; //defines border thickness
    self.profileCircle.layer.borderColor = [UIColor whiteColor].CGColor; //defines boreder color

}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [api_manager registerUserWithEmail];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
