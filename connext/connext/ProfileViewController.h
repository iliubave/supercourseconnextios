//
//  ProfileViewController.h
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-28.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnextAPIDelegate.h"

@interface ProfileViewController : UIViewController <ConnextApiDelegate>
@property (strong, nonatomic) IBOutlet UILabel *userFullname;
@property (strong, nonatomic) IBOutlet UILabel *userOccupation;
@property (strong, nonatomic) IBOutlet UILabel *currentEvent;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;

@end
