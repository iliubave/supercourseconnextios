//
//  MenuTableViewCell.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-26.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
