//
//  HashtagPaidAPI.m
//  launchboxTO
//
//  Created by Igar Liubavetskiy on 2014-12-10.
//  Copyright (c) 2014 Igar Liubavetskiy. All rights reserved.
//

#import "ConnextAPI.h"
#import <Firebase/Firebase.h>

@interface ConnextAPI ()

@end

@implementation ConnextAPI

- (void)submitGeoLocation:(NSString *)location forUserWithEmail:(NSString *)email {
    
}

- (void)registerUserWithEmail:(NSString *)email {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *linkedInID = [prefs stringForKey:@"userLinkedInID"];
    
    //Reference to the firebase database and allocating space for the event
    Firebase *ref = [[Firebase alloc] initWithUrl: [NSString stringWithFormat:@"https://connextsupercourse.firebaseio.com/users"]];
    
    //Make a user with the linkedin ID
    Firebase *usersRef = [ref childByAppendingPath: linkedInID]; //append linkedIn id here

    
    
    //Strings defined based on information retrieved from LinkedIn
    NSString *first = [prefs stringForKey:@"firstName"];
    NSString *last = [prefs stringForKey:@"lastName"];
    NSString *occupation = [prefs stringForKey:@"currentOccupation"];
    NSString *picURL = [prefs stringForKey:@"pic"];
    NSString *location = @"toronto";
    // NSString *eventName = @"Study";
    // NSString *eventLoc = @"toronto";
    
    
    //Putting all of the user information
    NSDictionary *userID =@{

                           @"first" : first,
                           @"last" : last,
                           @"job" : occupation,
                           @"profilePic" : picURL,
                           @"currLoc" : location,
                           // @"currEventName" : eventName,
                           // @"currEventLocation" : eventLoc,
                           };
    
    
    
    [usersRef setValue: userID];
}

- (void)registerUserWithID:(NSString *)userID forEventNamed:(NSString *)eventName {
    Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://connextsupercourse.firebaseIO.com/events/%@/attendees/",eventName]];
    
    NSDictionary *user = @{
                               @"userID" : userID,
                          };
    
    [ref setValue:user];
    
}

- (void)listOfAttendeesForEvent:(NSString *)eventName {
    Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://connextsupercourse.firebaseIO.com/events/%@/attendees/",eventName]];
    
    // Retrieve new posts as they are added to Firebase
    [ref observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        [self.delegate received:snapshot.value];
    }];
}

- (void)personalInformationForUserWithID:(NSString *)userID {
    Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://connextsupercourse.firebaseIO.com/users/%@",userID]];
    
    [ref observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        [self.delegate receivedPersons:snapshot.value];
    }];
}

- (void)registerUserWithID:(NSString *)userID fullName:(NSString *)fullName profilePictureURL:(NSString *)url occupation:(NSString *)occupation linkedinURL:(NSString *)linkedinURL skills:(NSDictionary *)skills connections:(NSDictionary *)connections{
    
    //Reference to the firebase database and allocating space for the event
    Firebase *ref = [[Firebase alloc] initWithUrl: [NSString stringWithFormat:@"https://connextsupercourse.firebaseio.com/users/%@",userID]];
    
    if(url == nil) {
        url = [NSString stringWithFormat:@""];
    }
    //Putting all of the user information
    NSDictionary *user =@{
                            
                            @"full_name": fullName,
                            @"occupation" : occupation,
                            @"profilePic" : url,
                            @"currentEventName" : @"Sample event",
                            @"linkedinURL" : linkedinURL,
                            @"skills" : skills,
                            @"connections" : connections
                            };
    [ref setValue: user];
    
    //Reference to the firebase database and allocating space for the event
    Firebase *ref2 = [[Firebase alloc] initWithUrl: [NSString stringWithFormat:@"https://connextsupercourse.firebaseio.com/events/Sample%%20event"]];
    
    NSDictionary *user2 = @{
                            userID : @"nil"
                            };
    Firebase *usersRef = [ref2 childByAppendingPath: [NSString stringWithFormat:@"attendees/%@",userID]];
    NSDictionary *users = @{
                            userID: user2
                            };
    [usersRef setValue: users];
}


@end
