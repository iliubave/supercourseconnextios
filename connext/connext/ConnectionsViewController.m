//
//  ConnectionsViewController.m
//  connext
//
//  Created by Igar Liubavetskiy on 2015-03-28.
//  Copyright (c) 2015 Igar Liubavetskiy. All rights reserved.
//

#import "ConnectionsViewController.h"
#import "RGCollectionViewCell.h"
#import "AsyncImageView.h"
#import "ConnextAPI.h"
#import "Person.h"

@interface ConnectionsViewController ()

@end

@implementation ConnectionsViewController {
    NSMutableArray *peopleIDs;
    NSMutableArray *persons;
    ConnextAPI *api_manager;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    peopleIDs = [[NSMutableArray alloc] init];
    persons = [[NSMutableArray alloc] init];
    
    api_manager = [[ConnextAPI alloc] init];
    api_manager.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [api_manager listOfAttendeesForEvent:@"Sample event"];
}

- (void)received:(NSData *)objectNotation {
    if(objectNotation != (id)[NSNull null]) {
        [peopleIDs removeAllObjects];
        [persons removeAllObjects];
        for(NSString *message in objectNotation) {
            [peopleIDs addObject:message];
        }
    }
    
    for(NSString *userID in peopleIDs) {
        [api_manager personalInformationForUserWithID:userID];
    }
}


- (void)receivedPersons:(NSData *)objectNotation {
    if(objectNotation != (id)[NSNull null]) {
        [persons addObject:[Person initWithDictionary:objectNotation]];
    }
    
    [self filterAttendies];
    [_peopleCollectionView reloadData];
}

- (void)filterAttendies {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary *connectionsDict = [prefs valueForKey:@"connections"];
    
    
    NSMutableArray *connections = [[NSMutableArray alloc] init];
    for(NSDictionary *connectionDict in connectionsDict) {
        NSString *connection = [NSString stringWithFormat:@"%@ %@",[connectionDict valueForKey:@"firstName"],[connectionDict valueForKey:@"lastName"]];
        [connections addObject:connection];
    }
    
    NSMutableArray *filtered = [[NSMutableArray alloc] init];
    for(Person *person in persons) {
        if([connections containsObject:person.fullName]) {
            [filtered addObject:person];
        }
    }
    
    persons = filtered;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  [persons count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RGCollectionViewCell *cell = (RGCollectionViewCell  *)[collectionView dequeueReusableCellWithReuseIdentifier:@"reuse" forIndexPath:indexPath];
    [self configureCell:cell withIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(RGCollectionViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.profileFullname.text = [(Person *)[persons objectAtIndex:indexPath.section] fullName];
    cell.profileOccupation.text = [(Person *)[persons objectAtIndex:indexPath.section] occupation];
    
    cell.profileImageView.image = nil;
    cell.profileImageView.imageURL = [NSURL URLWithString:[(Person *)[persons objectAtIndex:indexPath.section] profilePictureURL]];
    
    // case for the very first cell
    if(indexPath.section == 0) {
        [cell.progressView setProgress:[(Person *)[persons objectAtIndex:indexPath.section] percentage]/100.0 animated:YES];
        cell.shouldAnimate = false;
    }
    
    [cell.progressView setProgress:0 animated:NO];
    cell.shouldAnimate = false;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

